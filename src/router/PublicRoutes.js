import React from 'react';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from '../sites/Home';
import Footer from '../component/footer/Footer';
import LoginPage from '../sites/LoginPage';
import ScrollToTop from '../component/scrolltotop/ScrollToTop';
import RegisterPage from '../sites/RegisterPage';

function PublicRoutes() {
  return (
    <BrowserRouter>
      <div className="App">
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/register" element={<RegisterPage />} />
        </Routes>
        <ScrollToTop />
        <Footer />
      </div>
    </BrowserRouter>
  );
}

export default PublicRoutes;