import React from 'react';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import ScrollToTop from '../component/scrolltotop/ScrollToTop';
import Footer from '../component/footer/Footer';
import MovieDetailPage from '../sites/MovieDetailPage';
import { MovieProvider } from '../context/MovieContext';
import NetflixShow from '../sites/NetflixShow';

function PrivateRoutes() {
    return (
        <MovieProvider>
            <BrowserRouter>
                <div className="App">
                    <Routes>
                        <Route exact path="/" element={<NetflixShow />} />
                        <Route path="/detail" element={<MovieDetailPage />} />
                    </Routes>
                    <ScrollToTop />
                    <Footer />
                </div>
            </BrowserRouter>
        </MovieProvider>
    );
}

export default PrivateRoutes;