import React, { useState } from 'react';
import Tabs from './Tabs'
import { tablables } from "./constant.js"

function TabNavigation() {
    const [activeTab, setActiveTab] = useState(tablables.CANCEL_AT_ANY_TIME);

    const onClickTab = (tab) => {
        setActiveTab(tab);
    }

    return (
        <div>
            <Tabs activeTabName={activeTab} onClickTab={onClickTab} />
        </div>
    );
}

export default TabNavigation;