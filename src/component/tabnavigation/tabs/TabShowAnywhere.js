import React from 'react';
import { Link } from "react-router-dom"

function TabShowAnywhere() {
    return (
        <div>
            <div className='tab-2-content-top'>
                <p className="text-lg">Schaue Filme uns Serien überall und jederzeit für dich zugeschnitten</p>
                <Link to="/register" className="btn btn-lg">Für 30 Tage testen</Link>
            </div>
            <div className="tab-2-content-bottom">
                <div>
                    <img src={require("../../../images/tab-content-2-1.png")} alt="" />
                    <p className="text-md">Schaue auf deinem Gerät</p>
                    <p className="text-dark">Smart Tv, Playstation, XBox, Chromecast, Apple TV und mehr</p>
                </div>
                <div>
                    <img src={require("../../../images/tab-content-2-2.png")} alt="" />
                    <p className="text-md">Schaue jetzt oder downloade es für später</p>
                    <p className="text-dark">Verfügbar für Smartphones und tablets</p>
                </div>
                <div>
                    <img src={require("../../../images/tab-content-2-3.png")} alt="" />
                    <p className="text-md">Schaue jetzt oder downloade es für später</p>
                    <p className="text-dark">Verfügbar für Smartphones und tablets</p>
                </div>
            </div>
        </div>
    );
}

export default TabShowAnywhere;