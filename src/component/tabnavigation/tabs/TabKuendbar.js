import React from 'react';
import { Link } from "react-router-dom"

function TabKuendbar() {
    return (
        <div>
            <div className='tab-1-content-inner'>
                <div>
                    <p>Wenn du entscheidest das Netflix nichts für dich ist. Kein Problem. Ist jederzeit kündbar</p>
                    <Link to="/register" className="btn btn-lg">Für 30 Tage testen</Link>
                </div>
                <img src={require("../../../images/tab-content-1.png")} alt="" />
            </div>
        </div>
    );
}

export default TabKuendbar;