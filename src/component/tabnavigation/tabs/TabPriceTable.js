import React from 'react';
import { Link } from "react-router-dom"

function TabPriceTable() {
    return (
        <div>
            <div className='text-center'>
                <p className="text-lg">Wähle deinen Tarif und schaue überall alles</p>
                <Link to="/register" className="btn btn-lg">Für 30 Tage testen</Link>
            </div>
            <table className="table">
                <thead>
                    <tr>
                        <th></th>
                        <th>Basis</th>
                        <th>Standart</th>
                        <th>Premium</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>monatlicher Preis. Erster Monat ist kostenlos</td>
                        <td>6,99 €</td>
                        <td>11,99 €</td>
                        <td>15,99 €</td>
                    </tr>
                    <tr>
                        <td>HD Verfügbar</td>
                        <td><i className="fas fa-check" /> </td>
                        <td><i className="fas fa-times" /> </td>
                        <td><i className="fas fa-times" /> </td>
                    </tr>
                    <tr>
                        <td>Ultra HD Verfügbar</td>
                        <td><i className="fas fa-check" /> </td>
                        <td><i className="fas fa-check" /> </td>
                        <td><i className="fas fa-check" /> </td>
                    </tr>
                    <tr>
                        <td>Anzahl Geräte</td>
                        <td>1</td>
                        <td>2</td>
                        <td>3</td>
                    </tr>
                    <tr>
                        <td>Erster Monat frei</td>
                        <td><i className="fas fa-check" /> </td>
                        <td><i className="fas fa-check" /> </td>
                        <td><i className="fas fa-check" /> </td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
}

export default TabPriceTable;