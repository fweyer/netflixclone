import React from 'react';
import { tablables } from "./constant"
import TabPriceTable from './tabs/TabPriceTable';
import TabShowAnywhere from './tabs/TabShowAnywhere';
import TabKuendbar from './tabs/TabKuendbar';
import './Tabs.css'

function Tabs(props) {
    const { CANCEL_AT_ANY_TIME, WATCH_ANYWHERE, PICK_YOUR_PRICE } = tablables;
    const { activeTabName, onClickTab } = props;

    const renderTabTitle = (tabtitle, isActive, icon, id) => (
        <div
            onClick={() => onClickTab(tabtitle)}
            id={id}
            className={"tab-item " + (isActive ? 'tab-border' : '')}
        >
            <i className={icon}></i>
            <p>{tabtitle}</p>
        </div >
    );

    return (
        <div>
            <section className="tabs">
                <div className="container">
                    {renderTabTitle(CANCEL_AT_ANY_TIME, activeTabName == CANCEL_AT_ANY_TIME, "fas fa-door-open fa-3x", "tab-1")}
                    {renderTabTitle(WATCH_ANYWHERE, activeTabName === WATCH_ANYWHERE, "fas fa-tablet-alt fa-3x", "tab-2")}
                    {renderTabTitle(PICK_YOUR_PRICE, activeTabName === PICK_YOUR_PRICE, "fas fa-tags fa-3x", "tab-3")}
                </div>
            </section>
            {activeTabName === CANCEL_AT_ANY_TIME && (
                <section className="tab-content">
                    <div className="container">
                        <div id="tab-1-content" className={`tab-content-item ${activeTabName === CANCEL_AT_ANY_TIME && "show"}`}>
                            <TabKuendbar />
                        </div>
                    </div>
                </section>
            )}
            {activeTabName === WATCH_ANYWHERE && (
                <section className="tab-content">
                    <div className="container">
                        <div id="tab-2-content" className={`tab-content-item ${activeTabName === WATCH_ANYWHERE && "show"}`}>
                            <TabShowAnywhere />
                        </div>
                    </div>
                </section>
            )}
            {activeTabName === PICK_YOUR_PRICE && (
                <section className="tab-content">
                    <div className="container">
                        <div id="tab-3-content" className={`tab-content-item ${activeTabName === PICK_YOUR_PRICE && "show"}`}>
                            <TabPriceTable />
                        </div>
                    </div>
                </section>
            )}
        </div>
    );
}

export default Tabs;