
import React, { useContext } from 'react';
import useFetch from '../../hooks/useFetch';
import ShimmerAnimation from '../progress/ShimmerAnimation';
import MovieContext from '../../context/MovieContext';
import { useNavigate } from 'react-router-dom'
import "./Banner.css"

const base_url = "https://image.tmdb.org/t/p/original";

function Banner(props) {
    const { fetchUrl } = props;
    const { updateCurrentMovieState } = useContext(MovieContext)
    const { data, loading, error } = useFetch(`${process.env.REACT_APP_API}${fetchUrl}`)
    const navigate = useNavigate()

    if (loading) {
        return <ShimmerAnimation heightnum={448} />
    }

    if (error != null) {
        return <ShimmerAnimation heightnum={448} />
    }

    const handleClick = (movie) => {
        updateCurrentMovieState(movie)
        navigate('/detail', { replace: false })
    }

    const movieBanner = data.results[Math.floor(Math.random() * data.results.length)]

    return (
        <header className="banner"
            style={{
                backgroundSize: "cover",
                backgroundImage: `url(${base_url}${movieBanner?.backdrop_path})`,
                backgroundPosition: "center center"
            }}>
            <div className="banner-contents">
                <h1 className="banner-title">
                    {movieBanner?.title || movieBanner?.name || movieBanner?.original_name}
                </h1>
                <div className="banner-buttons">
                    <button className="banner-button" onClick={() => handleClick(movieBanner)}>Anzeigen</button>
                </div>
                <h1 className={`${movieBanner?.overview.length != 0 ? "banner-description" : ""}`}>
                    {movieBanner?.overview.length > 150 ? movieBanner?.overview.substr(0, 150 - 1) + "..." : movieBanner?.overview}</h1>
            </div>
            <div className="banner-fadeBottom"></div>
        </header>
    );
}

export default Banner;