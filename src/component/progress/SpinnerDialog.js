import React from 'react';
import { Spinner } from 'react-bootstrap';
import "./SpinnerDialog.scss"

function SpinnerDialog() {
    return (
        <section className='backgound-loading colum'>
            <div className="colum-box">
                <svg className="spinner" viewBox="0 0 50 50">
                    <circle className="path" cx="25" cy="25" r="20" fill="none" stroke-width="5"></circle>
                </svg>
                <h4 className='spinner-text'>wird geladen...</h4>
            </div>
        </section>
    );
}

export default SpinnerDialog;