import React from 'react';
import "./ShimmerAnimation.scss"

function ShimmerAnimation(props) {
    let { heightnum, isLargeRow } = props

    if (isLargeRow) {
        heightnum = 250
    }

    return (
        <div className="card">
            <div className="shimmerBG media" style={{ height: heightnum }}></div>
        </div >
    );
}

export default ShimmerAnimation;