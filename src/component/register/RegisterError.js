import React from 'react';

function RegisterError(props) {
    const { error } = props

    console.log(error)
    const errorMessage = error?.response?.data?.message

    const onRegisterClick = () => {
        window.location.reload(false);
    }

    return (
        <section className="backgound-loading colum ">
            <div className="colum-box">
                <h3 className="error-title">Es ist ein Fehler bei der Registrierung aufgetreten</h3>
                <h2 className="error-title">{errorMessage}</h2>
                <div className="btn btn-rounded" onClick={onRegisterClick}>zur Registrierung</div>
            </div>
        </section >
    );
}

export default RegisterError;