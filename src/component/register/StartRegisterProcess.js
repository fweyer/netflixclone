import React from 'react';
import SpinnerDialog from '../progress/SpinnerDialog';
import useRegister from '../../hooks/useRegister';
import RegisterError from './RegisterError'
import RegisterSuccess from './RegisterSuccess';


function StartRegisterProzess(props) {
    const { inputName, inputEmail, inputPassword } = props
    const { success, loading, error } = useRegister(inputName, inputEmail, inputPassword)

    if (loading) {
        return (<SpinnerDialog />)
    }

    if (success) {
        return (<RegisterSuccess />)
    }

    if (error != null) {
        return (<RegisterError error={error} />)
    }
}

export default StartRegisterProzess;