import React, { useState } from 'react';
import "./RegisterForm.css"
import { Link } from 'react-router-dom';
import StartRegisterProzess from './StartRegisterProcess';

function RegisterForm() {
    const [inputName, setInputName] = useState('')
    const [inputEmail, setInputEmail] = useState('')
    const [inputPassword, setInputPassword] = useState('')
    const [submit, setSubmit] = useState(false)

    const handleSubmitt = () => {
        if (inputName != null && inputEmail != null && inputPassword != null && inputName != "" && inputEmail != "" && inputPassword != "") {
            setSubmit(true)
        }
    }

    if (submit) {
        return (
            <StartRegisterProzess inputName={inputName} inputEmail={inputEmail} inputPassword={inputPassword} />
        );
    }

    return (
        <div>
            <main className='backgound'>
                <div className='form-content'>
                    <div className="form">
                        <div className="title">Registriere dich hier!</div>
                        <div className="subtitle">Bitte gib deine Daten ein.</div>
                        <div className="input-container ic1">
                            <input className="input" type="text" onInput={e => setInputName(e.target.value)} placeholder=" " />
                            <div className="cut"></div>
                            <label className="placeholder">Name</label>
                        </div>
                        <div className="input-container ic2">
                            <input className="input" type="text" onInput={e => setInputEmail(e.target.value)} placeholder=" " />
                            <div className="cut"></div>
                            <label className="placeholder">E-mail</label>
                        </div>
                        <div className="input-container ic2">
                            <input className="input" type="password" onInput={e => setInputPassword(e.target.value)} placeholder=" " />
                            <div className="cut"></div>
                            <label className="placeholder">Passwort</label>
                        </div>
                        <button type="text" onClick={handleSubmitt} className="submit">Registrieren</button>
                        <div className='form-fotter' >
                            <Link to="/login">Du hast einen Account? Dann logge dich ein!</Link>
                        </div>
                        <div className='form-fotter' >
                            <Link to="/">Zurück zur Startseite!</Link>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    );
}

export default RegisterForm;