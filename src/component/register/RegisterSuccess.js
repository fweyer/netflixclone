import React from 'react';
import { Link } from "react-router-dom"

function RegisterSuccess() {
    return (
        <section className="backgound-loading colum ">
            <div className="colum-box">
                <h3 className="error-title">Die Registrierung war erfolgreich!</h3>
                <Link to="/login" className="btn btn-rounded" >zur Login</Link>
            </div>
        </section >
    );
}

export default RegisterSuccess;