import React from 'react';
import "./Footer.css"

function Footer() {
    return (
        <footer className="footer">
            <p>Fragen? Rufe 0123/456789 an</p>
            <div className='footer-cols'>
                <ul>
                    <li><a href="#">FAQ</a></li>
                    <li><a href="#">Jobs</a></li>
                    <li><a href="#">HilfeCenter</a></li>
                </ul>
                <ul>
                    <li>Beispiel GmbH</li>
                    <li>Abenteuerstraße 16</li>
                    <li>12345 Musterhausen</li>
                </ul>
                <ul>
                    <li>beispiel@unternehmen.de</li>
                    <li>Tel: 01234/56789</li>
                    <li>Fax: 01234/56789</li>
                </ul>
                <ul>
                    <li>Deutsche Bank</li>
                    <li>DE1234 5678 9123 3456 78</li>
                    <li>BIG: DEGXB10</li>
                </ul>
            </div>
        </footer>
    );
}

export default Footer;