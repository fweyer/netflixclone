import React, { useContext } from 'react';
import { useNavigate } from 'react-router-dom'
import useFetch from '../../hooks/useFetch';
import ShimmerAnimation from '../progress/ShimmerAnimation';
import ScrollContainer from 'react-indiana-drag-scroll'
import MovieContext from '../../context/MovieContext';
import "./Row.css"

const base_url = "https://image.tmdb.org/t/p/original";

function Row(props) {
    const { title, fetchUrl, isLargeRow } = props;
    const { data, loading, error } = useFetch(`${process.env.REACT_APP_API}${fetchUrl}`)
    const { updateCurrentMovieState } = useContext(MovieContext)
    const navigate = useNavigate()

    const handleClick = (movie) => {
        updateCurrentMovieState(movie)
        navigate('/detail', { replace: false })
    }

    if (loading) {
        return <ShimmerAnimation heightnum={120} isLargeRow={isLargeRow} />
    }

    if (error != null) {
        console.log(error)
        return <ShimmerAnimation heightnum={120} isLargeRow={isLargeRow} />
    }

    const movies = data.results

    return (
        <div div className="row" >
            <h2>{title}</h2>
            <ScrollContainer className="row-posters" horizontal="true">
                {movies && movies.map((movie) => (
                    <img
                        onClick={() => handleClick(movie)}
                        key={movie.id}
                        className={`${isLargeRow ? "row-posterLarge" : "row-poster"}`}
                        src={`${base_url}${isLargeRow ? movie.poster_path : movie.backdrop_path}`}
                        alt={movie.name}
                    />
                ))}
            </ScrollContainer>
        </div>
    );

}

export default Row;


/*



if (trailerUrl) {
    setTrailerUrl("")
} else {
    movieTrailer(movie?.name || "")
        .then((url) => {
            const urlParams = new URLSearchParams(new URL(url).search)
            setTrailerUrl(urlParams.get("v"))
        }).catch((error) => { console.log(error) })
}

    const youtubeOps = {
    height: "390",
    width: "100%",
    playerVars: {
        autoplay: 1,
    }
}

{trailerUrl ? <Youtube videoId={trailerUrl} opts={youtubeOps} /> : <></>}
 
*/