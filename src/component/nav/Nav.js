import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom'
import "./Nav.css"

function Nav({ shouldShowBlackBalken }) {
    const [show, setShow] = useState(false)
    const [sigOut, setSignOut] = useState(false)
    const navigate = useNavigate()

    const handleSignOut = () => {
        window.localStorage.removeItem("access_token")
        window.localStorage.removeItem("refresh_token")
        setSignOut(true)
    }

    if (sigOut) {
        navigate('/', { replace: true })
        window.location.reload(false);
    }

    const handleBarVisibility = () => {
        if (window.scrollY > 100) {
            setShow(true)
        } else {
            setShow(false)
        }
    }

    useEffect(() => {
        window.addEventListener("scroll", handleBarVisibility)
        return () => { window.removeEventListener("scroll", handleBarVisibility) }
    }, [])


    return (
        <div className={`${show && shouldShowBlackBalken ? 'nav nav-black' : 'nav'}`} >
            <button className='btn btn-rounded nav-button' onClick={handleSignOut}>Logout</button>
        </div>
    );
}

export default Nav;