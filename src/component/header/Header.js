import React from 'react';
import "./Header.css"
import { Link } from "react-router-dom"

function Header() {
    return (
        <header className='showcase'>
            <div className='showcase-top'>
                <Link to="/login" className="btn btn-rounded">
                    Einloggen
                </Link>
            </div>
            <div className="showcase-content">
                <h1>Unbegrenzt Filme, Serien und mehr</h1>
                <p>Genießen Sie es, wo immer Sie möchten. Jederzeit kündbar.</p>
                <Link to="/register" className="btn-header btn-xl">
                    30 Tage kostenlos testen!
                    <i className="fas fa-chevron-right btn-icon"></i>
                </Link>
            </div>
        </header>
    );
}

export default Header;