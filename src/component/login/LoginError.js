import React from 'react';
import { Link } from "react-router-dom"

function LoginError(props) {
    const { onRetryClick } = props

    const onLoginClick = () => {
        window.location.reload(false);
    }

    return (
        <section className="backgound-loading colum ">
            <div className="colum-box">
                <h2 className="error-title">Es ist ein Fehler beim Login aufgetreten</h2>
                <div className="btn btn-rounded btn-retry" onClick={() => onRetryClick()} >Erneut versuchen</div>
                <div to="/login" className="btn btn-rounded" onClick={onLoginClick}>zum Login</div>
            </div>
        </section >
    );
}

export default LoginError;