import React, { useState } from 'react';
import useLogin from '../../hooks/useLogin';
import LoginError from './LoginError';
import SpinnerDialog from '../progress/SpinnerDialog';
import { useNavigate } from 'react-router-dom'


function StartLoginProzess(props) {
    const { inputEmail, inputPassword } = props
    const { success, loading, error } = useLogin(inputEmail, inputPassword)
    const [retry, setRetry] = useState(false)
    const navigate = useNavigate()

    const passRetryfunc = () => {
        setRetry(true)
    }

    if (retry) {
        return (<StartLoginProzess />)
    }

    if (loading) {
        return (
            <SpinnerDialog />
        )
    }

    if (success) {
        //Page einmal neu laden damit die neuen Routes greifen
        navigate('/', { replace: true })
        window.location.reload(false);
    }

    if (error != null) {
        return (<LoginError onRetryClick={passRetryfunc} />)
    }
}

export default StartLoginProzess;