import React, { useState } from 'react';
import "./LoginForm.css"
import { Link } from 'react-router-dom';
import StartLoginProzess from './StartLoginProzess';

function LoginForm() {

    const [inputEmail, setInputEmail] = useState('')
    const [inputPassword, setInputPassword] = useState('')
    const [submit, setSubmit] = useState(false)

    const handleSubmitt = () => {
        if (inputEmail != null && inputPassword != null && inputEmail != "" && inputPassword != "") {
            setSubmit(true)
        }
    }

    if (submit) {
        return (
            <StartLoginProzess inputEmail={inputEmail} inputPassword={inputPassword} />
        );
    }

    return (
        <div>
            <main className='backgound'>
                <div className="form">
                    <div className="title">Wilkommen</div>
                    <div className="subtitle">Bitte logge dich ein!</div>
                    <div className="input-container ic1">
                        <input className="input" value={inputEmail} onInput={e => setInputEmail(e.target.value)} type="text" placeholder=" " />
                        <div className="cut"></div>
                        <label className="placeholder">E-Mail</label>
                    </div>
                    <div className="input-container ic2">
                        <input className="input" value={inputPassword} onInput={e => setInputPassword(e.target.value)} type="password" placeholder=" " />
                        <div className="cut"></div>
                        <label className="placeholder">Passwort</label>
                    </div>
                    <button type="text" className="submit" onClick={handleSubmitt}>Einloggen</button>
                    <div className='form-fotter' >
                        <Link to="/register">Noch keinen Account? Registriere dich hier!</Link>
                    </div>
                    <div className='form-fotter'>
                        <Link to="/">Zurück zur Startseite!</Link>
                    </div>
                </div>
            </main>
        </div>
    );
}

export default LoginForm;