import React, { useContext, useState } from 'react';
import MovieContext from '../../context/MovieContext';
import Youtube from 'react-youtube';
import movieTrailer from 'movie-trailer';
import "./MovieDetail.css"


function MovieDetail() {
    const base_url = "https://image.tmdb.org/t/p/original";
    const { movie } = useContext(MovieContext)
    const [trailerUrl, setTrailerUrl] = useState("")

    window.scrollTo({ top: 0 })

    const handleClick = () => {
        if (trailerUrl) {
            setTrailerUrl("")
        } else {
            movieTrailer(movie?.name || "")
                .then((url) => {
                    const urlParams = new URLSearchParams(new URL(url).search)
                    setTrailerUrl(urlParams.get("v"))
                }).catch((error) => { console.log(error) })
        }
    }

    const youtubeOps = {
        height: "390",
        width: "100%",
        playerVars: {
            autoplay: 1,
        }
    }

    return (
        <div>
            <header className="movie"
                style={{
                    backgroundSize: "cover",
                    backgroundImage: `url(${base_url}${movie?.backdrop_path})`,
                    backgroundPosition: "center center"
                }}>
                <div className="movie-contents">
                    <h1 className="movie-title">
                        {movie?.title || movie?.name || movie?.original_name}
                    </h1>
                    <div className="movie-buttons">
                        <button className="movie-button" onClick={handleClick}>Abspielen</button>
                    </div>
                    <h1 className={`${movie?.overview.length != 0 ? "movie-description" : ""}`}>{movie?.overview}</h1>
                </div>
                <div className="movie-fadeBottom"></div>
            </header>
            {trailerUrl ? <Youtube videoId={trailerUrl} opts={youtubeOps} /> : <></>}
        </div>
    );
}

export default MovieDetail;