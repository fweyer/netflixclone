import { useState, useEffect } from 'react';
import axios from 'axios';

function useTokenRefresh(inputEmail, inputPassword) {
    const [error, setError] = useState(null)
    const [isLoggedIn, setIsLoggedIn] = useState(false)

    const tokenRefresh = window.localStorage.getItem("refresh_token");

    useEffect(() => {
        const fetchData = async () => {
            try {
                const responseAuth = await axios.post(`${process.env.REACT_APP_API}/token/refresh`, tokenRefresh)
                const token = responseAuth.data.access_token
                window.localStorage.setItem("access_token", token)
                setIsLoggedIn(true)
            } catch (error) {
                setError(error)
            }
        };
        fetchData()
    }, [])
    return { isLoggedIn, error }
}

export default useTokenRefresh;

