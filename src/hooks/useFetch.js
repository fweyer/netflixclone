import { useState, useEffect } from 'react';
import axios from 'axios';

function useFetch(url) {
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState(null)
    const [data, setData] = useState(null)

    const token = window.localStorage.getItem("access_token");

    const createHeadersforRequest = (token) => {
        return {
            headers: {
                Authorization: 'Bearer ' + token,
                "Content-Type": "application/x-www-form-urlencoded"
            }
        }
    }

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await axios.get(url, createHeadersforRequest(token))
                setData(response.data)
                setLoading(false)
            } catch (error) {
                setError(error)
                setLoading(false)
            }
        };
        fetchData()
    }, [])
    return { data, loading, error }
}

export default useFetch;