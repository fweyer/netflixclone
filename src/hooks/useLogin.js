import { useState, useEffect } from 'react';
import axios from 'axios';

function useLogin(inputEmail, inputPassword) {
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState(null)
    const [success, setSuccess] = useState(false)


    const authData = new URLSearchParams({
        email: inputEmail, //gave the values directly for testing
        password: inputPassword,
    })

    useEffect(() => {
        const fetchData = async () => {
            try {
                const responseAuth = await axios.post(`${process.env.REACT_APP_API}/api/login`, authData)
                const token = responseAuth.data.access_token
                const tokenRefresh = responseAuth.data.refresh_token
                window.localStorage.setItem("access_token", token)
                window.localStorage.setItem("refresh_token", tokenRefresh)
                setSuccess(true)
                setLoading(false)
            } catch (error) {
                setError(error)
                setLoading(false)
            }
        };
        fetchData()
    }, [])
    return { success, loading, error }
}

export default useLogin;