import { useState, useEffect } from 'react';
import axios from 'axios';

function useRegister(inputName, inputEmail, inputPassword) {
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState(null)
    const [success, setSuccess] = useState(false)


    const registerRequest = {
        userName: inputName,
        email: inputEmail,
        password: inputPassword,
    }

    useEffect(() => {
        const fetchData = async () => {
            try {
                await axios.post(`${process.env.REACT_APP_API}/api/register`, registerRequest)
                setSuccess(true)
                setLoading(false)
            } catch (error) {
                setError(error)
                setLoading(false)
            }
        };
        fetchData()
    }, [])
    return { success, loading, error }
}

export default useRegister;