import { createContext, useState } from "react";

const MovieContext = createContext()

export const MovieProvider = ({ children }) => {
    const [movie, setMovie] = useState(null)

    const updateCurrentMovieState = (movie) => setMovie(movie)

    return <MovieContext.Provider value={{ movie, updateCurrentMovieState }}>{children}</MovieContext.Provider>
}

export default MovieContext;