import React from 'react';
import useTokenRefresh from './hooks/useTokenRefresh';
import PrivateRoutes from './router/PrivateRoutes';
import PublicRoutes from './router/PublicRoutes'
import './App.css';

function App() {
  const { isLoggedIn, error } = useTokenRefresh()

  if (error != null) console.log(error)

  if (isLoggedIn) return (<PrivateRoutes />)   // routed zu den pages, wenn ein User eingeloggt ist
  else return (<PublicRoutes />)              // routed zu den pages, wenn kein User eingelogt ist
}

export default App;
