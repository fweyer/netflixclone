import React from 'react';
import Row from '../component/row/Row';
import Banner from '../component/banner/Banner'
import api from "../api/api"
import Nav from '../component/nav/Nav';

function NetflixShow() {
    return (
        <>
            <Nav shouldShowBlackBalken={true} />
            <Banner fetchUrl={api.fetchNetflixOriginals} />
            <Row title="Netflix Originale" fetchUrl={api.fetchNetflixOriginals} isLargeRow />
            <Row title="Trends" fetchUrl={api.fetchTrending} />
            <Row title="Top Bewertet" fetchUrl={api.fetchTopRated} />
            <Row title="Action" fetchUrl={api.fetchActionMovies} />
            <Row title="Comedy" fetchUrl={api.fetchComedyMovies} />
            <Row title="Horror" fetchUrl={api.fetchHorrorMovies} />
            <Row title="Romanzen" fetchUrl={api.fetchRomanceMovies} />
        </>
    );
}

export default NetflixShow;
