import React from 'react';
import RegisterForm from '../component/register/RegisterForm';

function RegisterPage() {
    return (
        <>
            <RegisterForm />
        </>
    );
}

export default RegisterPage;