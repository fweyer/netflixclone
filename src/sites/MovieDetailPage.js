import React from 'react';
import MovieDetail from "../component/moviedetail/MovieDetail"
import Nav from '../component/nav/Nav';


function MovieDetailPage() {
    return (
        <>
            <Nav />
            <MovieDetail />
        </>
    );
}

export default MovieDetailPage;