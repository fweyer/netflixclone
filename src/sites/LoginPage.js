import React from 'react';
import LoginForm from '../component/login/LoginForm';

function LoginPage() {
    return (
        <>
            <LoginForm />
        </>
    );
}

export default LoginPage;