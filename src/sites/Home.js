import React from 'react';
import Header from '../component/header/Header';
import TabNavigation from '../component/tabnavigation/TabNavigation';


function Home() {
    return (
        <>
            <Header />
            <TabNavigation />
        </>
    );
}

export default Home;