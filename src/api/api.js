const api = {
    fetchTrending: `/api/movie/trending`,
    fetchNetflixOriginals: `/api/movie/netflixoriginals`,
    fetchTopRated: `/api/movie/topradet`,
    fetchActionMovies: `/api/movie/action`,
    fetchComedyMovies: `/api/movie/comedy`,
    fetchHorrorMovies: `/api/movie/horror`,
    fetchRomanceMovies: `/api/movie/romance`,
}

export default api;